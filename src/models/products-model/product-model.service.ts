import { getManager, getRepository } from "typeorm";
import { ProductNewDto } from "./dto/product-dto";
import { ProductEntity } from "./entity/product-entity";


const productModel = ()=> getRepository(ProductEntity)


export class ProductModelService{
    constructor(){}
    
    
    public async getProducts(userId:Number):Promise<ProductEntity[]>{
        return await productModel().find({
            where:{userId}
        })
       
    }

    public async createNewProductAndUpdate(payload:ProductNewDto | any):Promise<ProductEntity>{
        return  await productModel().save(payload);
    }
    
    public async productfindById(productId:number, userId:number):Promise<ProductEntity | undefined>{
        return  await productModel().findOne({where:[
            {id:productId},
            {userId}
        ]});
    }
   
    public async deleteById(id:number):Promise<Boolean>{
        return Boolean(await productModel().delete(id));
    }
}
