import { Entity, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";


@Entity("products")
export class ProductEntity {

    @PrimaryGeneratedColumn()
    id:Number;

    @Column()
    name:String;

    @Column()
    category:String

    @Column()
    image:String;

    @Column()
    price:Number;

    @Column()
    stock:Number;

    @Column()
    @ManyToOne("users")
    userId:Number;

    @Column()
    @CreateDateColumn()
    createdAt:Date;

    @Column()
    @UpdateDateColumn()
    updatedAt:Date;
}