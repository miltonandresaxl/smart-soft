import * as jf from "joiful";

interface InNewPoduct{
    name:string;
    category:string;
    price:number;
    image:string;
    stock:number;
    userId:number;
}


export class ProductNewDto {
    @jf.string().required()
    name: string;
    @jf.string().required()
    category: string;
    @jf.string().required()
    image: string;
    @jf.number().required()
    price: number;
    @jf.number().required()
    stock: number;
    @jf.number().required()
    userId: number;

    constructor({name, category, image, price, stock, userId}:InNewPoduct){
        this.name=name;
        this.category=category;
        this.price=price;
        this.stock=stock;
        this.userId= userId;
        this.image=image;
    }
}

interface InUpdatePoduct{
    name:string;
    price:number;
    image:string;
    stock:number;
}

export class ProductUpdateDTO {
    @jf.string().required()
    name: string;
    @jf.string().required()
    image: string;
    @jf.number().required()
    price: number;
    @jf.number().required()
    stock: number;

    constructor({name, image, price, stock,}:InUpdatePoduct){
        this.name=name;
        this.price=price;
        this.stock=stock;
        this.image=image;
    }
}