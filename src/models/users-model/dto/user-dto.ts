import * as jf from 'joiful';

interface InRegister{
    name:string;
    lastname:string;
    email:string;
    password:string;
}

export class UserRegisterDTO{
    @jf.string().required()
    name:string;

    @jf.string().required()
    lastname:string

    @jf.string().email().required()
    email:string;

    @jf.string().required()
    password:string;

    constructor({name, lastname, email, password}: InRegister){
        this.name = name;
        this.password = password;
        this.lastname = lastname;
        this.email = email;
    }
}


interface InLogin{
    email:string;
    password:string;
}

export class UserLoginDTO{

    @jf.string().email().required()
    email:string;

    @jf.string().required()
    password:string;

    constructor({email, password}:InLogin){
        this.email = email;
        this.password = password;
    }
}