import { getManager, getRepository } from "typeorm";
import { UserLoginDTO, UserRegisterDTO } from "./dto/user-dto";
import { UserEntity } from "./entity/user-entity";


const userModel = ()=> getRepository(UserEntity)


export class UserModelService{
    constructor(){}
    
    
    public async getUsersById(id:Number):Promise<UserEntity | undefined>{
        return await userModel().findOne({
            select:["createdAt","name","lastname","email"],
            where:{id}
        });
    }

    public async registerUser(payload:UserRegisterDTO):Promise<UserEntity | undefined>{
        const user =  await userModel().save(payload);
        return await this.getUsersById(user.id)
    }
    public async getUserByEmail(email:String):Promise<UserEntity | undefined>{
        return  await userModel().findOne({
            select:["id", "password"],
            where:{email}
        });
    }
   
}
