import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm";


@Entity("users")
export class UserEntity {

    @PrimaryGeneratedColumn()
    id:Number;

    @Column()
    name:String;

    @Column()
    lastname:String;


    @Column({unique:true})
    email:String;

    @Column()
    password:String;

    @Column()
    @CreateDateColumn()
    createdAt:Date;

    @Column()
    @UpdateDateColumn()
    updatedAt:Date;
}