import { Request, Response } from "express";
import { HttpStatus, log } from "../config";
import { HandlerException } from "../handlerErrors";
import useService from "../services/user.service";



export const register = async({body}:Request, res:Response) : Promise<Response>=>{
    try {
        return res.status(HttpStatus.CREATED).json({
            status:HttpStatus.CREATED,
            data: await useService.registerUser(body)
        });
    } catch (error) {
        return  HandlerException(res, error);
    }
    
}

export const login = async(req:Request, res:Response) : Promise<Response>=>{
   
    try {
        return res.status(HttpStatus.OK).json({
            status:HttpStatus.OK,
            data: await useService.loginUser(req.body)
        });
    } catch (error) {
        return  HandlerException(res, error);
    }
    
}

export const getUsersById = async(req:Request, res:Response) : Promise<Response>=>{
    
    return res.status(HttpStatus.OK).json({
        status:HttpStatus.OK,
        data: await useService.getUsersById(res.locals.user.id)
    })
}