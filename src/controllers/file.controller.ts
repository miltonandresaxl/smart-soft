import { Request, Response } from "express";
import { HttpStatus } from "../config";
import { HandlerException } from "../handlerErrors";
import { uploadFile } from "../helpers";


export const fileUpload = async(req:Request, res:Response):Promise<Response>=>{
    try {
        return res.status(HttpStatus.OK).json({
            status:HttpStatus.OK,
            data: await uploadFile(req.file)
        });
    } catch (error) {
        return  HandlerException(res, error);
    }
    
}