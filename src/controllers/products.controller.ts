import { Request, Response } from "express";
import { HttpStatus, log } from "../config";
import { HandlerException } from "../handlerErrors";
import productService from "../services/product.service";

export const getAllByUserProducts = async (req:Request, res:Response)=>{
    try {
        return res.status(HttpStatus.OK).json({
            status:HttpStatus.OK,
            data: await productService.getAllProductByUser(res.locals.user.id)
        });
    } catch (error) {
        return  HandlerException(res, error);
    }

}


export const newProduct =async ({body}:Request, res:Response)=>{
    try {
        
        return res.status(HttpStatus.OK).json({
            status:HttpStatus.OK,
            data: await productService.createNewProduct({...body, userId:res.locals.user.id})
        });
    } catch (error) {
        return  HandlerException(res, error);
    }
}

export const getProductById =async ({params}:Request, res:Response)=>{
    try {
        
        return res.status(HttpStatus.OK).json({
            status:HttpStatus.OK,
            data: await productService.getProductById(Number(params.id), res.locals.user.id)
        });
    } catch (error) {
        return  HandlerException(res, error);
    }
}
export const updateProduct =async ({body, params}:Request, res:Response)=>{
    try {
        
        return res.status(HttpStatus.OK).json({
            status:HttpStatus.OK,
            data: await productService.updateProduct(body, Number(params.id), res.locals.user.id)
        });
    } catch (error) {
        return  HandlerException(res, error);
    }
}