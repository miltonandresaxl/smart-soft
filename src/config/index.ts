import { bold } from 'chalk';
import { customLog, sequencialColor } from 'termx';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';


import CODES from '../config/status.json';


export const log = customLog(sequencialColor(), bold('SMARTSOFT'));

export const HttpStatus = {...CODES};


export const cryptText =(text:string)=> bcrypt.hashSync(text, Number(process.env.SALT));

export const compareCryptText =( text:string, valueBcrypt:string | undefined)=> bcrypt.compareSync(text, valueBcrypt || "");


export const encodeToken=(text:Object)=> jwt.sign(text, String(process.env.JWT_S), {
    expiresIn:3600000 * 2
});

export const decodeToken = (text:string) => jwt.verify(text, String(process.env.JWT_S))