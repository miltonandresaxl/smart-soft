import * as dotenv from 'dotenv';

export const config = ()=>  {
    const envPath = 'src/env/.env';
    dotenv.config({ path: envPath });
}