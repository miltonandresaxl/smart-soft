import { Router }  from "express";
import { getUsersById, login, register} from "../controllers/user.controller";
import { authMiddleware } from "../middlewares/auth.routes";
import multer from 'multer';
import { fileUpload } from "../controllers/file.controller";
import { fileMiddleware } from "../middlewares/file_validate";


const uploadMulter = multer(); 

const _files = Router();


_files.post('/upload', [authMiddleware, uploadMulter.single('image'), fileMiddleware], fileUpload)

export default _files;
