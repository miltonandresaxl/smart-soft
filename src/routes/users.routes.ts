import { Router }  from "express";
import { getUsersById, login, register} from "../controllers/user.controller";
import { authMiddleware } from "../middlewares/auth.routes";

const _user = Router();


_user.post('/register', register);
_user.post('/login', login);
_user.get('/',[authMiddleware],getUsersById);

export default _user;