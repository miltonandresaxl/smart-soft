import { Router }  from "express";
import { getAllByUserProducts, getProductById, newProduct, updateProduct,  } from "../controllers/products.controller";
import { authMiddleware } from "../middlewares/auth.routes";
const _prod = Router();

_prod.post('/new',[authMiddleware], newProduct)
_prod.get('/all',[authMiddleware], getAllByUserProducts)
_prod.get('/:id',[authMiddleware], getProductById)
_prod.put('/:id',[authMiddleware], updateProduct)

export default _prod;