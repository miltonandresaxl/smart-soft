import 'reflect-metadata';

import  express from "express";
import cors from 'cors';
import morgan from "morgan";
import { createConnection } from "typeorm";

//import owners

import _prod from './routes/products.routes';
import _user from "./routes/users.routes";
import { config } from './config/enviroments';
import _files from './routes/files.routes';
 
config();


createConnection();

//initialize
const app = express();



// middleware
app
    .use(cors())
    .use(morgan('dev'))
    .use(express.json())
//routes
    .use('/user',_user)
    .use('/product', _prod)
    .use('/img',_files)






export default app;