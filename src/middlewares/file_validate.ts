import { NextFunction, Request, Response } from "express";
import { HttpStatus } from "../config";

export const fileMiddleware = (req:Request, res:Response, next:NextFunction)=>{
    try {
        if(!req.file || Object.keys(req.file).length ===0 || !req.file) throw Error("Image cannot be null. Please verify and try again")
        next()
    } catch (error) {
        return  res.status(HttpStatus.UNAUTHORIZED).json({
            status:HttpStatus.UNAUTHORIZED,
            message:error.message
        })
    }
}