import { NextFunction, Request, Response } from "express";
import { decodeToken, HttpStatus } from "../config";

export const authMiddleware = (req:Request, res:Response, next:NextFunction)=>{
    try {
        const header = req.headers.authorization;

        if(!header || !(header as String).split(" ")[1]) throw Error('session token is required');


        const token:any = decodeToken((header as String).split(" ")[1]) 
        if(!new Date().getTime() >=  token['exp']) throw new Error('token expired');
        res.locals.user=token.user;
        next()
    } catch (error) {
        return  res.status(HttpStatus.UNAUTHORIZED).json({
            status:HttpStatus.UNAUTHORIZED,
            message:error.message
        })
    }
}