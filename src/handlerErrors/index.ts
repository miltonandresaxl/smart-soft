import {  Response } from "express";
export const HandlerException=async(res:Response, error:any): Promise<Response>=>{

    const {status, message} = JSON.parse(error.message);
    return res.status(status).json({
        status,
        message
    })
}