import { UserLoginDTO, UserRegisterDTO } from "../models/users-model/dto/user-dto";
import { UserModelService } from "../models/users-model/user-model.service";
import  * as jf from "joiful";
import { UserEntity } from "../models/index.entities";
import { compareCryptText, cryptText, decodeToken, encodeToken, HttpStatus, log } from "../config";
const userModelService = new UserModelService();


export default { 
    registerUser: async (payload:UserRegisterDTO): Promise<UserEntity | undefined> => {
        
        const {error} = jf.validate(new UserRegisterDTO(payload));
        if(error){
            throw Error(JSON.stringify({
                status:HttpStatus.FORBBIDEN,
                message:error.message
            }))
        }

        if(await userModelService.getUserByEmail(payload.email)) throw Error(JSON.stringify({
          status:HttpStatus.FORBBIDEN,
          message:"user alright exists!"
      }))



        payload.password = cryptText(payload.password);
        return await userModelService.registerUser(payload);
      },
      
      getUsersById:async (id:Number): Promise<UserEntity  |  undefined> => {
        return await userModelService.getUsersById(id);
      },

      loginUser: async (payload:UserLoginDTO):Promise<String> => {
          const { error } = jf.validate(new UserLoginDTO(payload));
          if(error) throw Error(JSON.stringify({
            status:HttpStatus.FORBBIDEN,
            message:error.message
          }));

          const userResult = await userModelService.getUserByEmail(payload.email);
          if(!compareCryptText(payload.password, userResult?.password.toString()) || !userResult)  throw Error(JSON.stringify({
            status:HttpStatus.FORBBIDEN,
            message:"Incorrect username or password.!"
          }))

          return  encodeToken({user:{id:userResult.id}});
      }
 };
