import { ProductEntity } from "../models/index.entities";
import { ProductNewDto, ProductUpdateDTO } from "../models/products-model/dto/product-dto";
import { ProductModelService } from "../models/products-model/product-model.service";
import * as jf from 'joiful';
import { HttpStatus, log } from "../config";

const productModelService = new ProductModelService();


export default{
    createNewProduct:async (payload:ProductNewDto) :Promise<ProductEntity | undefined>=>{
        
        const { error } = jf.validate(new ProductNewDto(payload));
      
        if(error) throw Error(JSON.stringify({
            status:HttpStatus.FORBBIDEN,
            message:error.message
        }))
        return await productModelService.createNewProductAndUpdate(payload);
    },

    getAllProductByUser: async (userId:number):Promise<ProductEntity[] | undefined> =>{
        return await productModelService.getProducts(userId);
    },

    getProductById: async (productId:number, userId:number):Promise<ProductEntity | undefined> =>{
        const product = await  productModelService.productfindById(productId, userId);
        if(!product) throw Error(JSON.stringify({
            status:HttpStatus.FORBBIDEN,
            message:"Prodcut not found. Please verify and try again"
        }))
        return product;
    },

    updateProduct: async (payload: ProductUpdateDTO,productId:number, userId:number):Promise<ProductEntity | undefined> =>{
        const { error } = jf.validate(new ProductUpdateDTO(payload)) 
        if(error) throw Error(JSON.stringify({
            status:HttpStatus.FORBBIDEN,
            message:error.message
        }))
        const product = await  productModelService.productfindById(productId, userId);

        return await  productModelService.createNewProductAndUpdate( {
            ...product,
            ...payload
        })
    },
    deleteProduct: async (productId:number, userId:number):Promise<Boolean> =>{
        const product = await  productModelService.productfindById(productId, userId);
        if(!product) throw Error(JSON.stringify({
            status:HttpStatus.FORBBIDEN,
            message:"Prodcut not found. Please verify and try again"
        }))
        return await  productModelService.deleteById(productId)
    },
}