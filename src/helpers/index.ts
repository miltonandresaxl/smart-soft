import S3 from 'aws-sdk/clients/s3';
import fs from 'fs';
import { log } from '../config';








export  const uploadFile = async (file:any)=>{
    try {
        const s3 = new S3({
            region:process.env.AWS_BUCKET_REGION,
            accessKeyId:process.env.AWS_ACCESS_KEY,
            secretAccessKey:process.env.AWS_SECRET_KEY
        })
        
        const uploadParams:any = {
            Bucket:process.env.AWS_BUCKET_NAME,
            Body:file.buffer,
            Key:`${Date.now()}_${file.originalname}`.trim()
        }
    
        const { Location : url }:any = await s3.upload(uploadParams).promise();
        return url;
    } catch (error) {
        log('error ->',error.message)
    }
   
}